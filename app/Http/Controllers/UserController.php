<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;

class UserController extends Controller
{
    //
    public function index(){
    	$banners = User::orderBy('name')->paginate(10);
    	return $banners;
    }
}
